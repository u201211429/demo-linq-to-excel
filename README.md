# ¿Qué es? #
¡Un demo de la biblioteca Linq a Excel!

# ¿Cómo hacerlo funcionar?#

Clonar el proyecto y luego bajar este motor de base de datos:

https://www.microsoft.com/en-us/download/details.aspx?id=23734

Mover documento demo en la carpeta excel a la misma carpeta donde se encuentra el ejecutable luego de compilar.